#/bin/bash

curl -s "https://api.kraken.com/0/public/AssetPairs" \
 | grep -Eo 'altname\":\"[a-zA-Z0-9\.]*\"' \
 | awk -F: '{print $2}' \
 | tr -d '[:punct:]' \
 | awk '{ system ("curl -s https://api.kraken.com/0/public/Ticker?pair=" $1)}' \
 | grep -Eo 'result\":\{\"[a-zA-Z0-9]*\":\{\"a\":\[\"[0-9]*.[0-9]*' \
 | awk -F'"' '{print $3 " "  $7}'