# DevOps



### 1. Docker
I have created multistage Docker build and scanned it with suggested Anchore.
Please refer to Dockerfile
```
        Policy Evaluation - litecoin:0.0.2
-----------------------------------------------------------

Image Digest: sha256:651e14d3776a025f2974e4f16cba4ca9410d785e1b8252082f3b91fb722b81f6
Full Tag: localhost:5000/litecoin:0.0.2
Image ID: cb80244586a1edb6c3d50f9a5324818b9b7945c3ddd76a5e21a1aeb6551b984f
Status: pass
Last Eval: 2022-01-23T08:39:24Z
Policy ID: 2c53a13c-1765-11e8-82ef-23527761d060
Final Action: warn
Final Action Reason: policy_evaluation

Gate                   Trigger            Detail                                                                                                                                                                              Status        
dockerfile             instruction        Dockerfile directive 'HEALTHCHECK' not found, matching condition 'not_exists' check                                                                                                 warn          
vulnerabilities        package            MEDIUM Vulnerability found in os package type (dpkg) - perl-base (CVE-2020-16156 - http://people.ubuntu.com/~ubuntu-security/cve/CVE-2020-16156)                                    warn          
vulnerabilities        package            MEDIUM Vulnerability found in os package type (dpkg) - libsystemd0 (fixed in: 248.3-1ubuntu8.2)(CVE-2021-3997 - http://people.ubuntu.com/~ubuntu-security/cve/CVE-2021-3997)        warn          
vulnerabilities        package            MEDIUM Vulnerability found in os package type (dpkg) - libudev1 (fixed in: 248.3-1ubuntu8.2)(CVE-2021-3997 - http://people.ubuntu.com/~ubuntu-security/cve/CVE-2021-3997)           warn          


Cleaning up docker container: 7155-inline-anchore-engine
```

### 2. k8s
I have created helm chart named **litecoin-ss** for ease of deployemnt rollback and release tracking.
This helm chart is deployed to DigitalOcean k8s cluster by gitlab ci pipeline

Here is initial deployment

```
NAME    	NAMESPACE	REVISION	UPDATED                                	STATUS  	CHART            	APP VERSION
litecoin	litecoin 	1       	2022-01-22 20:21:57.752355873 +0000 UTC	deployed	litecoin-ss-0.0.1	0.18.1
```

```
NAME                         READY   STATUS    RESTARTS   AGE
pod/litecoin-litecoin-ss-0   1/1     Running   0          12m

NAME                                    READY   AGE
statefulset.apps/litecoin-litecoin-ss   1/1     12m

NAME                                      TYPE                                  DATA   AGE
secret/default-token-98gsz                kubernetes.io/service-account-token   3      12m
secret/litecoin-litecoin-ss-token-sjj6f   kubernetes.io/service-account-token   3      12m
secret/sh.helm.release.v1.litecoin.v1     helm.sh/release.v1                    1      12m

NAME                                  SECRETS   AGE
serviceaccount/default                1         12m
serviceaccount/litecoin-litecoin-ss   1         12m

NAME                                                                STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS       AGE
persistentvolumeclaim/litecoin-litecoin-ss-litecoin-litecoin-ss-0   Bound    pvc-3818071e-1902-4694-a458-4d3182faaca5   20Gi       RWO            do-block-storage   12m
```

Here is upgrade deployemnt after docker image base image version bump

```
NAME    	NAMESPACE	REVISION	UPDATED                                	STATUS  	CHART            	APP VERSION
litecoin	litecoin 	2       	2022-01-23 08:45:09.822087804 +0000 UTC	deployed	litecoin-ss-0.0.2	0.18.1
```

```
NAME                         READY   STATUS    RESTARTS   AGE
pod/litecoin-litecoin-ss-0   1/1     Running   0          5m54s

NAME                                    READY   AGE
statefulset.apps/litecoin-litecoin-ss   1/1     12h

NAME                                      TYPE                                  DATA   AGE
secret/default-token-98gsz                kubernetes.io/service-account-token   3      12h
secret/litecoin-litecoin-ss-token-sjj6f   kubernetes.io/service-account-token   3      12h
secret/sh.helm.release.v1.litecoin.v1     helm.sh/release.v1                    1      12h
secret/sh.helm.release.v1.litecoin.v2     helm.sh/release.v1                    1      6m34s

NAME                                  SECRETS   AGE
serviceaccount/default                1         12h
serviceaccount/litecoin-litecoin-ss   1         12h

NAME                                                                STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS       AGE
persistentvolumeclaim/litecoin-litecoin-ss-litecoin-litecoin-ss-0   Bound    pvc-3818071e-1902-4694-a458-4d3182faaca5   20Gi       RWO            do-block-storage   12h
```


### 3. Pipeline
I have build 5 stages pipeline in gitlab ci.
Pipeline use secrets variables for docker and DigitalOcean K8s cluster stored in Gitlab CI/CD variables

1. Build - build docker image and pushes to docker.io
2. Container scanning runs gitlab vurnability scanner [https://docs.gitlab.com/ee/user/application_security/container_scanning/#configuration](https://docs.gitlab.com/ee/user/application_security/container_scanning/#configuration)
3. Deploy - deploy to DigitalOcean k8s cluster using helm
4. Validate - simple validation by checking litecoin api response
5. Rollback - in case of failed validation rollback use helm to previous release

Configuration is in **.gitlab-ci.yml**. 

Pipeline was ran twice to test upgrade.

Sucessfully Executed pipelines details can be found [https://gitlab.com/ssawulski/devops/-/pipelines](https://gitlab.com/ssawulski/devops/-/pipelines).  


### 4. Script kiddies
I have created **get_tickers_prices.sh** script that uses curl, awk, grep, tr.   
I have used man pages for  grep, awk, tr and some stackoverflow.   
Script is obtaning all AssetPairs data from api then use it to query Ticker pair endpoint for each AssetPair.   
lastly it prints tickers and asking price. 
It takes about 2min to finish as its making api calls one by one. 

It could definettely be optimized as I'm not that super profitient with those tools prefere to use python when possible.  

```
curl -s "https://api.kraken.com/0/public/AssetPairs" \
 | grep -Eo 'altname\":\"[a-zA-Z0-9\.]*\"' \
 | awk -F: '{print $2}' \
 | tr -d '[:punct:]' \
 | awk '{ system ("curl -s https://api.kraken.com/0/public/Ticker?pair=" $1)}' \
 | grep -Eo 'result\":\{\"[a-zA-Z0-9]*\":\{\"a\":\[\"[0-9]*.[0-9]*' \
 | awk -F'"' '{print $3 " "  $7}'
```


<details><summary>Script Output Click to expand</summary>
1INCHEUR 1.42300
1INCHUSD 1.61500
AAVEAUD 205.89000
AAVEETH 0.06110
AAVEEUR 130.08000
AAVEGBP 109.06000
AAVEUSD 147.44000
AAVEXBT 0.0041970
ADAAUD 1.506700
ADAETH 0.000446200
ADAEUR 0.952080
ADAGBP 0.797490
ADAUSD 1.078571
ADAUSDT 1.07847200
ADAXBT 0.000030700
ALGOETH 0.000391700
ALGOEUR 0.834360
ALGOGBP 0.699680
ALGOUSD 0.946020
ALGOXBT 0.000026920
ANKREUR 0.0549800
ANKRGBP 0.0460900
ANKRUSD 0.0623100
ANKRXBT 0.0000017800
ANTETH 0.0018660
ANTEUR 3.97330
ANTUSD 4.50140
ANTXBT 0.000128090
ASTREUR 0.09200
ASTRUSD 0.10300
ATOMAUD 41.136900
ATOMETH 0.01216100
ATOMEUR 25.923800
ATOMGBP 21.735100
ATOMUSD 29.347000
ATOMXBT 0.00083660
AUDJPY 81.779
AUDUSD 0.71748
AVAXEUR 52.52000
AVAXUSD 59.51000
AXSEUR 44.35100
AXSUSD 50.20600
BADGEREUR 9.41000
BADGERUSD 10.67100
BALETH 0.004890
BALEUR 10.42000
BALUSD 11.81000
BALXBT 0.00033700
BANDEUR 3.10700
BANDUSD 3.52700
BATETH 0.000297500
BATEUR 0.633840
BATJPY 81.79000
BATUSD 0.718230
BATXBT 0.000020440
BCHAUD 410.310000
BCHETH 0.12180000
BCHEUR 259.380000
BCHGBP 217.590000
BCHJPY 33499.000000
BCHUSD 294.000000
BCHUSDT 294.050000
BCHXBT 0.00837000
BNCEUR 0.97000
BNCUSD 1.11000
BNTEUR 2.04700
BNTGBP 1.71500
BNTUSD 2.32000
BNTXBT 0.000066100
CHZEUR 0.157100
CHZUSD 0.178000
COMPETH 0.05030
COMPEUR 106.9500
COMPUSD 121.2400
COMPXBT 0.0034490
CQTEUR 0.35100
CQTUSD 0.39800
CRVETH 0.00120500
CRVEUR 2.56400
CRVUSD 2.90400
CRVXBT 0.00008270
CTSIEUR 0.390700
CTSIUSD 0.443000
DAIEUR 0.88250000
DAIUSD 1.00009000
DAIUSDT 1.00002000
DASHEUR 85.629000
DASHUSD 96.982000
DASHXBT 0.00276000
DOTAUD 25.73590
DOTETH 0.00760400
DOTEUR 16.21100
DOTGBP 13.58150
DOTUSD 18.37010
DOTUSDT 18.373400
DOTXBT 0.0005228400
DYDXEUR 5.53900
DYDXUSD 6.28100
ENJEUR 1.33300
ENJGBP 1.11900
ENJUSD 1.51100
ENJXBT 0.0000429700
EOSETH 0.00092700
EOSEUR 1.971900
EOSUSD 2.234900
EOSUSDT 2.236300
EOSXBT 0.00006370
ETHAUD 3373.170000
ETHCHF 2211.95000
ETHDAI 2419.528000
ETHUSDC 2418.330000
ETHUSDT 2416.87000
EURAUD 1.58266
EURCAD 1.42296
EURCHF 1.03555
EURGBP 0.83788
EURJPY 129.466
EWTEUR 5.090000
EWTGBP 4.268000
EWTUSD 5.777000
EWTXBT 0.000164190
FILAUD 27.00700
FILETH 0.0079500
FILEUR 16.93400
FILGBP 14.20700
FILUSD 19.20900
FILXBT 0.000546700
FLOWETH 0.0021100
FLOWEUR 4.48600
FLOWGBP 3.75900
FLOWUSD 5.08500
FLOWXBT 0.000144600
GHSTEUR 1.816700
GHSTGBP 1.555700
GHSTUSD 2.059800
GHSTXBT 0.0000586800
GLMREUR 6.55800
GLMRUSD 7.43100
GNOETH 0.10100000
GNOEUR 215.950000
GNOUSD 244.630000
GNOXBT 0.00696000
GRTAUD 0.5995400
GRTETH 0.000174000
GRTEUR 0.3706600
GRTGBP 0.3105400
GRTUSD 0.4200000
GRTXBT 0.000011960
ICXETH 0.000284400
ICXEUR 0.605300
ICXUSD 0.686200
ICXXBT 0.000019510
INJEUR 3.86700
INJUSD 4.38300
KAREUR 1.95200
KARUSD 2.20600
KAVAETH 0.0014200
KAVAEUR 3.04210
KAVAUSD 3.44150
KAVAXBT 0.000097620
KEEPETH 0.00016910
KEEPEUR 0.360630
KEEPUSD 0.408680
KEEPXBT 0.000011630
KILTEUR 1.85000
KILTUSD 2.11000
KINTEUR 19.14000
KINTUSD 21.70000
KNCETH 0.0006220
KNCEUR 1.32650
KNCUSD 1.50030
KNCXBT 0.000042770
KSMAUD 266.57000
KSMDOT 10.40000
KSMETH 0.078970
KSMEUR 168.64000
KSMGBP 141.48000
KSMUSD 191.04000
KSMXBT 0.00543400
LINKAUD 22.363000
LINKETH 0.006605360
LINKEUR 14.103500
LINKGBP 11.828000
LINKJPY 1827.10000
LINKUSD 15.970000
LINKUSDT 15.9915200
LINKXBT 0.000454460
LPTEUR 20.88000
LPTGBP 17.50000
LPTUSD 23.69000
LPTXBT 0.000673600
LRCEUR 0.650900
LRCUSD 0.737800
LSKETH 0.000662740
LSKEUR 1.415617
LSKUSD 1.604652
LSKXBT 0.000045543
LTCAUD 152.95000
LTCETH 0.045350
LTCGBP 81.01516
LTCUSDT 109.518810
LUNAEUR 56.23000
LUNAUSD 63.72000
MANAETH 0.000832700
MANAEUR 1.7771100
MANAUSD 2.0146600
MANAXBT 0.000057290
MATICEUR 1.353500
MATICGBP 1.133600
MATICUSD 1.533600
MATICXBT 0.0000436000
MINAEUR 2.11000
MINAGBP 1.77000
MINAUSD 2.38000
MINAXBT 0.000067900
MIREUR 1.13400
MIRUSD 1.28400
MKREUR 1595.50000
MKRGBP 1337.70000
MKRUSD 1808.10000
MKRXBT 0.0514800
MOVREUR 74.627200
MOVRUSD 84.710400
NANOETH 0.000835530
NANOEUR 1.782409
NANOUSD 2.021558
NANOXBT 0.000057489
OCEANEUR 0.5570000
OCEANGBP 0.4666000
OCEANUSD 0.6309000
OCEANXBT 0.000017960
OGNEUR 0.281000
OGNUSD 0.318300
OMGETH 0.001759460
OMGEUR 3.748345
OMGJPY 486.70000
OMGUSD 4.245117
OMGXBT 0.000120929
OXTETH 0.00009690
OXTEUR 0.206620
OXTUSD 0.234280
OXTXBT 0.000006660
OXYEUR 0.24200
OXYUSD 0.27300
PAXGETH 0.765022000
PAXGEUR 1629.410000
PAXGUSD 1842.770000
PAXGXBT 0.052478000
PERPEUR 6.93000
PERPUSD 7.85700
PHAEUR 0.21800
PHAUSD 0.24600
QTUMETH 0.00231290
QTUMEUR 4.930890
QTUMUSD 5.582140
QTUMXBT 0.00015910
RARIEUR 7.95000
RARIGBP 6.78000
RARIUSD 9.02000
RARIXBT 0.000256900
RAYEUR 3.27600
RAYUSD 3.71600
RENEUR 0.253700
RENGBP 0.238100
RENUSD 0.287200
RENXBT 0.0000082000
REPV2ETH 0.005040
REPV2EUR 10.74400
REPV2USD 12.17400
REPV2XBT 0.00034600
SANDEUR 2.665100
SANDGBP 2.236400
SANDUSD 3.019200
SANDXBT 0.0000860600
SCETH 0.0000038900
SCEUR 0.008250
SCUSD 0.009360
SCXBT 0.0000002666
SDNEUR 0.82600
SDNUSD 0.93900
SHIBEUR 0.0000175300
SHIBUSD 0.0000198500
SNXAUD 5.339000
SNXETH 0.001580
SNXEUR 3.36300
SNXGBP 2.814000
SNXUSD 3.80300
SNXXBT 0.00010850
SOLEUR 83.72000
SOLGBP 70.28000
SOLUSD 94.87000
SOLXBT 0.002707600
SRMEUR 1.95500
SRMGBP 1.64000
SRMUSD 2.21500
SRMXBT 0.0000631000
STORJETH 0.00042990
STORJEUR 0.914380
STORJUSD 1.039440
STORJXBT 0.0000295900
SUSHIEUR 3.95000
SUSHIGBP 3.33000
SUSHIUSD 4.48000
SUSHIXBT 0.000127200
TBTCETH 15.089000
TBTCEUR 31699.50000
TBTCUSD 39977.40000
TBTCXBT 1.01812000
TRXETH 0.0000235400
TRXEUR 0.0501730
TRXUSD 0.0568470
TRXXBT 0.0000016179
UNIETH 0.0047000
UNIEUR 10.00200
UNIUSD 11.33500
UNIXBT 0.000322600
USDCAUD 1.39580000
USDCEUR 0.88270000
USDCGBP 0.73930000
USDCHF 0.91313
USDCUSD 1.00010000
USDCUSDT 1.00000000
USDTAUD 1.395100
USDTCAD 1.25460000
USDTCHF 0.913220
USDTEUR 0.88280000
USDTGBP 0.73940000
USDTJPY 113.961000
USDTZUSD 1.00010000
WAVESETH 0.00370590
WAVESEUR 7.893400
WAVESUSD 8.943400
WAVESXBT 0.00025463
WBTCEUR 31071.50000
WBTCUSD 35221.70000
WBTCXBT 1.0021000
XBTAUD 49018.20000
XBTCHF 32135.40000
XBTDAI 35185.10000
XBTUSDC 35147.95000
XBTUSDT 35139.50000
XDGEUR 0.116563200
XDGUSD 0.132052200
XDGUSDT 0.132050000
XETCXETH 0.01000100
XETCXXBT 0.00068700
XETCZEUR 21.27700000
XETCZUSD 24.12500000
XETHXXBT 0.068800
XETHZCAD 3038.62000
XETHZEUR 2134.42000
XETHZGBP 1787.25000
XETHZJPY 275759.00000
XETHZUSD 2421.27000
XLTCXXBT 0.00311100
XLTCZEUR 96.53000
XLTCZJPY 12471.00000
XLTCZUSD 109.29000
XMLNXETH 0.02207000
XMLNXXBT 0.00151900
XMLNZEUR 47.011000
XMLNZUSD 53.338000
XREPXETH 0.005060
XREPXXBT 0.000347
XREPZEUR 10.77400
XREPZUSD 12.21400
XRPAUD 0.83299000
XRPETH 0.0002468000
XRPGBP 0.44102000
XRPUSDT 0.59617000
XTZAUD 4.080700
XTZETH 0.00120910
XTZEUR 2.575800
XTZGBP 2.160700
XTZUSD 2.918800
XTZXBT 0.00008310
XXBTZCAD 44102.80000
XXBTZEUR 31009.10000
XXBTZGBP 25985.00000
XXBTZJPY 4003592.000
XXBTZUSD 35140.50000
XXDGXXBT 0.000003770
XXLMXXBT 0.000005470
XXLMZAUD 0.26869000
XXLMZEUR 0.16946400
XXLMZGBP 0.14195000
XXLMZUSD 0.19202900
XXMRXXBT 0.00430800
XXMRZEUR 133.45000000
XXMRZUSD 151.31000000
XXRPXXBT 0.000016970
XXRPZCAD 0.74885000
XXRPZEUR 0.52602000
XXRPZJPY 67.98800000
XXRPZUSD 0.59600000
XZECXXBT 0.002670
XZECZEUR 82.56900
XZECZUSD 93.58000
YFIAUD 32032.00000
YFIETH 9.50000
YFIEUR 20208.00000
YFIGBP 16925.00000
YFIUSD 22877.00000
YFIXBT 0.651800
ZEURZUSD 1.13292
ZGBPZUSD 1.35309
ZRXEUR 0.45800
ZRXGBP 0.38400
ZRXUSD 0.51900
ZRXXBT 0.0000147600
ZUSDZCAD 1.25499
ZUSDZJPY 113.919
</details>

### 5.Script grown-ups
I have reproduced same functionality as in kiddies script using python 3.8 **get_tickers_prices.py**. 
Only standard libraries were used for portability and improved speed with multithreading as tasks are I/O Bound. 
[https://docs.python.org/3/library/concurrent.futures.html#threadpoolexecutor-example](https://docs.python.org/3/library/concurrent.futures.html#threadpoolexecutor-example). 

I have wrote unittest test_get_tickers_prices.py and generated coverage report.  
```
test_get_api_data (__main__.MainTestCase) ... ok
test_get_tickers_urls (__main__.MainTestCase) ... ok
test_print_tickers_prices (__main__.MainTestCase) ... ok
test_threads (__main__.MainTestCase) ... ok
test_threads_exception (__main__.MainTestCase) ... ok
test_main (__main__.MainTestCase) ... ok

----------------------------------------------------------------------
Ran 6 tests in 0.007s

OK
```

```
Name                         Stmts   Miss  Cover   Missing
----------------------------------------------------------
get_tickers_prices.py           32      0   100%
test_get_tickers_prices.py      56      0   100%
----------------------------------------------------------
TOTAL                           88      0   100%
```
 
### 6. Terraform
As requested I have created module for creation of iam resources with toggled prefix

```
Terraform used the selected providers to generate the following execution plan. Resource actions are
indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # module.iam_resources.aws_iam_group.group will be created
  + resource "aws_iam_group" "group" {
      + arn       = (known after apply)
      + id        = (known after apply)
      + name      = "prod-ci-group"
      + path      = "/"
      + unique_id = (known after apply)
    }

  # module.iam_resources.aws_iam_group_membership.membership will be created
  + resource "aws_iam_group_membership" "membership" {
      + group = "prod-ci-group"
      + id    = (known after apply)
      + name  = "prod-ci-membership"
      + users = [
          + "prod-ci-user",
        ]
    }

  # module.iam_resources.aws_iam_group_policy_attachment.policy_attach will be created
  + resource "aws_iam_group_policy_attachment" "policy_attach" {
      + group      = "prod-ci-group"
      + id         = (known after apply)
      + policy_arn = (known after apply)
    }

  # module.iam_resources.aws_iam_policy.policy will be created
  + resource "aws_iam_policy" "policy" {
      + arn         = (known after apply)
      + description = "Policy to allow assume role"
      + id          = (known after apply)
      + name        = "prod-ci-policy"
      + path        = "/"
      + policy      = jsonencode(
            {
              + Statement = [
                  + {
                      + Action   = [
                          + "sts:AssumeRole",
                        ]
                      + Effect   = "Allow"
                      + Resource = "arn:aws:iam::000000000000:role/prod-ci-role"
                    },
                ]
              + Version   = "2012-10-17"
            }
        )
      + policy_id   = (known after apply)
      + tags_all    = (known after apply)
    }

  # module.iam_resources.aws_iam_role.iam_role will be created
  + resource "aws_iam_role" "iam_role" {
      + arn                   = (known after apply)
      + assume_role_policy    = jsonencode(
            {
              + Statement = [
                  + {
                      + Action    = "sts:AssumeRole"
                      + Effect    = "Allow"
                      + Principal = {
                          + AWS = "000000000000"
                        }
                      + Sid       = ""
                    },
                ]
              + Version   = "2012-10-17"
            }
        )
      + create_date           = (known after apply)
      + force_detach_policies = false
      + id                    = (known after apply)
      + managed_policy_arns   = (known after apply)
      + max_session_duration  = 3600
      + name                  = "prod-ci-role"
      + name_prefix           = (known after apply)
      + path                  = "/"
      + tags_all              = (known after apply)
      + unique_id             = (known after apply)

      + inline_policy {
          + name   = (known after apply)
          + policy = (known after apply)
        }
    }

  # module.iam_resources.aws_iam_user.user will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "prod-ci-user"
      + path          = "/"
      + tags_all      = (known after apply)
      + unique_id     = (known after apply)
    }

Plan: 6 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

module.iam_resources.aws_iam_user.user: Creating...
module.iam_resources.aws_iam_group.group: Creating...
module.iam_resources.aws_iam_role.iam_role: Creating...
module.iam_resources.aws_iam_group.group: Creation complete after 1s [id=prod-ci-group]
module.iam_resources.aws_iam_user.user: Creation complete after 1s [id=prod-ci-user]
module.iam_resources.aws_iam_group_membership.membership: Creating...
module.iam_resources.aws_iam_group_membership.membership: Creation complete after 1s [id=prod-ci-membership]
module.iam_resources.aws_iam_role.iam_role: Creation complete after 2s [id=prod-ci-role]
module.iam_resources.aws_iam_policy.policy: Creating...
module.iam_resources.aws_iam_policy.policy: Creation complete after 2s [id=arn:aws:iam::000000000000:policy/prod-ci-policy]
module.iam_resources.aws_iam_group_policy_attachment.policy_attach: Creating...
module.iam_resources.aws_iam_group_policy_attachment.policy_attach: Creation complete after 1s [id=prod-ci-group-20220122193557690000000001]

Apply complete! Resources: 6 added, 0 changed, 0 destroyed.
```