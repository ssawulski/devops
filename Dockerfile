ARG LVERSION=0.18.1

FROM ubuntu:21.10 AS init

ARG LVERSION
ARG FINGERPRINT=FE3348877809386C

#Its multistage so we dont care about adding layers in init stage
ADD https://download.litecoin.org/litecoin-$LVERSION/linux/litecoin-$LVERSION-x86_64-linux-gnu.tar.gz \
    https://download.litecoin.org/litecoin-$LVERSION/linux/litecoin-$LVERSION-linux-signatures.asc /


#Its multistage so we dont care about adding layers in init stage
RUN  apt-get update  && apt-get install  -y  gpg
#As per https://download.litecoin.org/README-HOWTO-GPG-VERIFY-TEAM-MEMBERS-KEY.txt
RUN gpg --keyserver keyserver.ubuntu.com  --recv-key $FINGERPRINT 
RUN gpg --verify litecoin-$LVERSION-linux-signatures.asc
RUN echo $(grep litecoin-$LVERSION-x86_64-linux-gnu.tar.gz litecoin-$LVERSION-linux-signatures.asc | awk '{print $1}') litecoin-$LVERSION-x86_64-linux-gnu.tar.gz | sha256sum -c
RUN  tar zxvf litecoin-$LVERSION-x86_64-linux-gnu.tar.gz


FROM ubuntu:21.10
ARG LVERSION
ARG LUSER=litecoin
ENV LUSER=$LUSER
RUN useradd -m -s /usr/sbin/nologin -u 1001 -U $LUSER
COPY --from=init --chown=$LUSER:$LUSER /litecoin-$LVERSION/ /home/$LUSER/
USER $LUSER
CMD /home/$LUSER/bin/litecoind -printtoconsole
