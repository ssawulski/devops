#!/usr/bin/env python3

"""
    This script is replicating get_tickers_prices.sh functionality
    using only python standard libraries
    Due to many I/O bound http calls
    Multithreading was used for speed improvement
"""
import concurrent.futures
import json
import urllib.request


PUBLIC_API = "https://api.kraken.com/0/public/"


def get_api_data(url, timeout):
    with urllib.request.urlopen(url, timeout=timeout) as conn:
        return json.load(conn)


def get_tickers_urls(url):
    api_data = get_api_data(url, 10)

    tickers = ['{}{}{}'.format(PUBLIC_API, 'Ticker?pair=',
               api_data['result'][x]['altname'])
               for x in api_data['result']]

    return tickers


# https://docs.python.org/3/library/concurrent.futures.html
def threads(url_list):
    tickers_data = {}
    with concurrent.futures.ThreadPoolExecutor(max_workers=None) as executor:
        future_to_url = {executor.submit(get_api_data, url, 10): url for
                         url in url_list}
        for future in concurrent.futures.as_completed(future_to_url):
            url = future_to_url[future]
            try:
                data = future.result()
            except Exception as e:
                print('%s generated an exception: %s' % (url, e))
            else:
                tickers_data.update(data.get('result', {}))
    return tickers_data


def print_tickers_prices(tickers_data):
    for k, v in tickers_data.items():
        print(k, v['a'][0])


def main():
    url_list = get_tickers_urls(PUBLIC_API+"AssetPairs")
    tickers_data = threads(url_list)
    print_tickers_prices(tickers_data)


if __name__ == '__main__':
    main()  # pragma: no cover
