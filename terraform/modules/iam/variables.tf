variable "prefix" {
  description = "Prefix to be used in the resources created"
  type        = string
  default = "prod-ci"
}

variable "toggle_prefix" {
    description = "Preffix toggle switch"
    type = bool
    default = true
}