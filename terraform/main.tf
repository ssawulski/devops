provider "aws" {
  region = "eu-west-3"
}

module "iam_resources" {
  source = "./modules/iam"

  toggle_prefix = true
}
