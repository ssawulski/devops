#!/usr/bin/env python3

from io import StringIO
from unittest.mock import patch

import get_tickers_prices
import json
import sys
import unittest

ASSETS_TEST_DATA = {'error': [],
                    'result': {'1INCHEUR': {'altname': '1INCHEUR',
                                            'wsname': '1INCH/EUR',
                                            'aclass_base': 'currency',
                                            'base': '1INCH',
                                            'aclass_quote': 'currency',
                                            'quote': 'ZEUR',
                                            'lot': 'unit',
                                            'pair_decimals': 3,
                                            'lot_decimals': 8,
                                            'lot_multiplier': 1,
                                            'leverage_buy': [],
                                            'leverage_sell': [],
                                            'fees': [[0, 0.26],
                                                     [50000, 0.24],
                                                     [100000, 0.22],
                                                     [250000, 0.2],
                                                     [500000, 0.18],
                                                     [1000000, 0.16],
                                                     [2500000, 0.14],
                                                     [5000000, 0.12],
                                                     [10000000, 0.1]],
                                            'fees_maker': [[0, 0.16],
                                                           [50000, 0.14],
                                                           [100000, 0.12],
                                                           [250000, 0.1],
                                                           [500000, 0.08],
                                                           [1000000, 0.06],
                                                           [2500000, 0.04],
                                                           [5000000, 0.02],
                                                           [10000000, 0.0]],
                                            'fee_volume_currency': 'ZUSD',
                                            'margin_call': 80,
                                            'margin_stop': 40,
                                            'ordermin': '1'},
                               '1INCHUSD': {'altname': '1INCHUSD',
                                            'wsname': '1INCH/USD',
                                            'aclass_base': 'currency',
                                            'base': '1INCH',
                                            'aclass_quote': 'currency',
                                            'quote': 'ZUSD',
                                            'lot': 'unit',
                                            'pair_decimals': 3,
                                            'lot_decimals': 8,
                                            'lot_multiplier': 1,
                                            'leverage_buy': [],
                                            'leverage_sell': [],
                                            'fees': [[0, 0.26],
                                                     [50000, 0.24],
                                                     [100000, 0.22],
                                                     [250000, 0.2],
                                                     [500000, 0.18],
                                                     [1000000, 0.16],
                                                     [2500000, 0.14],
                                                     [5000000, 0.12],
                                                     [10000000, 0.1]],
                                            'fees_maker': [[0, 0.16],
                                                           [50000, 0.14],
                                                           [100000, 0.12],
                                                           [250000, 0.1],
                                                           [500000, 0.08],
                                                           [1000000, 0.06],
                                                           [2500000, 0.04],
                                                           [5000000, 0.02],
                                                           [10000000, 0.0]],
                                            'fee_volume_currency': 'ZUSD',
                                            'margin_call': 80,
                                            'margin_stop': 40,
                                            'ordermin': '1'}}}

URLS_TEST_DATA = ['https://api.kraken.com/0/public/Ticker?pair=1INCHEUR',
                  'https://api.kraken.com/0/public/Ticker?pair=1INCHUSD']

TICKER1_TEST_DATA = {'error': [],
                     'result': {'1INCHEUR': {'a': ['1.48100', '195', '195.000'],
                                             'b': ['1.47400', '2257', '2257.000'],
                                             'c': ['1.48600', '12.54888739'],
                                             'v': ['47401.42333494', '1556913.14651074'],
                                             'p': ['1.46632', '1.64279'],
                                             't': [198, 1754],
                                             'l': ['1.32400', '1.32400'],
                                             'h': ['1.64400', '1.71900'],
                                             'o': '1.61000'}}}

TICKER2_TEST_DATA = {'error': [],
                     'result': {'1INCHUSD': {'a': ['1.67600', '1257', '1257.000'],
                                             'b': ['1.67100', '181', '181.000'],
                                             'c': ['1.67900', '74.85000000'],
                                             'v': ['39682.08106779', '492810.84525025'],
                                             'p': ['1.62064', '1.82178'],
                                             't': [241, 1581],
                                             'l': ['1.50600', '1.50600'],
                                             'h': ['1.89200', '1.94700'],
                                             'o': '1.89200'}}}

TICKERS_TEST_DATA = {'1INCHEUR': {'a': ['1.48100', '195', '195.000'],
                                  'b': ['1.47400', '2257', '2257.000'],
                                  'c': ['1.48600', '12.54888739'],
                                  'v': ['47401.42333494', '1556913.14651074'],
                                  'p': ['1.46632', '1.64279'],
                                  't': [198, 1754],
                                  'l': ['1.32400', '1.32400'],
                                  'h': ['1.64400', '1.71900'],
                                  'o': '1.61000'},
                     '1INCHUSD': {'a': ['1.67600', '1257', '1257.000'],
                                  'b': ['1.67100', '181', '181.000'],
                                  'c': ['1.67900', '74.85000000'],
                                  'v': ['39682.08106779', '492810.84525025'],
                                  'p': ['1.62064', '1.82178'],
                                  't': [241, 1581],
                                  'l': ['1.50600', '1.50600'],
                                  'h': ['1.89200', '1.94700'],
                                  'o': '1.89200'}}

PRINT_RESULT = '1INCHEUR 1.48100\n1INCHUSD 1.67600\n'


class MainTestCase(unittest.TestCase):

    @patch('urllib.request.urlopen', return_value=StringIO(json.dumps(ASSETS_TEST_DATA)))
    def test_get_api_data(self, umock):
        self.maxDiff = None
        self.assertEqual(get_tickers_prices.get_api_data(
            'url', 1), ASSETS_TEST_DATA)

    @patch('get_tickers_prices.get_api_data', return_value=ASSETS_TEST_DATA)
    def test_get_tickers_urls(self, gmock):
        result = URLS_TEST_DATA
        self.maxDiff = None
        self.assertEqual(get_tickers_prices.get_tickers_urls('url'), result)

    @patch('sys.stdout', new_callable=StringIO)
    def test_print_tickers_prices(self, omock):
        get_tickers_prices.print_tickers_prices(TICKERS_TEST_DATA)
        self.maxDiff = None
        self.assertEqual(omock.getvalue(), PRINT_RESULT)

    @patch('get_tickers_prices.get_api_data',
           side_effect=[TICKER1_TEST_DATA, TICKER2_TEST_DATA])
    def test_threads(self, gmock):
        self.maxDiff = None
        self.assertEqual(get_tickers_prices.threads(
            URLS_TEST_DATA), TICKERS_TEST_DATA)

    @patch('sys.stdout', new_callable=StringIO)
    @patch('get_tickers_prices.get_api_data', side_effect=Exception('Some Exception'))
    def test_threads_exception(self, gmock, omock):
        get_tickers_prices.threads(URLS_TEST_DATA)
        result = ("https://api.kraken.com/0/public/Ticker?pair=1INCHEUR generated an exception: Some Exception\n"
                  "https://api.kraken.com/0/public/Ticker?pair=1INCHUSD generated an exception: Some Exception\n")
        self.maxDiff = None
        self.assertEqual(omock.getvalue(), result)

    @patch('sys.stdout', new_callable=StringIO)
    @patch('urllib.request.urlopen',
           side_effect=[StringIO(json.dumps(ASSETS_TEST_DATA)),
                        StringIO(json.dumps(TICKER1_TEST_DATA)),
                        StringIO(json.dumps(TICKER2_TEST_DATA))])
    def test_main(self, umock, omock):
        get_tickers_prices.main()
        self.maxDiff = None
        self.assertEqual(omock.getvalue(), PRINT_RESULT)


def suite():
    suite = unittest.TestSuite()
    suite.addTest(MainTestCase('test_get_api_data'))
    suite.addTest(MainTestCase('test_get_tickers_urls'))
    suite.addTest(MainTestCase('test_print_tickers_prices'))
    suite.addTest(MainTestCase('test_threads'))
    suite.addTest(MainTestCase('test_threads_exception'))
    suite.addTest(MainTestCase('test_main'))
    return suite


if __name__ == '__main__':
    result = unittest.TextTestRunner(verbosity=2).run(suite())
    sys.exit(not result.wasSuccessful())
